Contents

-Introduction
-Requirements
-Installation
-Configuration
-Troubleshooting
-FAQ
-Contributors

#Introduction#

This E-Library is used to share and read books in PDF format.

Users can log in and submit books to be read, read books, and share books.
Administrators known as Super Users are able to monitor and approve 
pending users and books, as well as complaints that people might have about a book.
If a Super User approves of three complaints for a book, the book is banned.
If a User has submitted two books that have been banned, then he gets banned.

#Requirements#

 This project only requires Java


#Installation#
	Open gitProj322.jar

#Configuration#

	Permissions
		Guests
		-Browse Books
		-Read Book Summaries
		-Read Book Reviews
		-Register to become a User
		
		Users
		-All Guest privileges
		-Read Book Content
		-Rate and review Books
		-Complain about Books
		-Add Books to Favorites
		-Share Books with other Users
		-Submit Books to be approved
		
		Super Users
		-All User privileges
		-Approve Pending Books
		-Approve Pending Users
		-Approve Complaints

#Troubleshooting#
	
	The screen sometimes blacks out, often when logging out. If this happens, resize the window.

#FAQ#

	Q:How come I cannot submit certain books
	A:The cover needs to be in JPG format and the book file needs to be in PDF format.

#Contributors#

Max Inciong
Binod Tamalsina
Hasnain Khan