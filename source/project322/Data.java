/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project322;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author binod
 */
public class Data {

    //DATABASE path
    private static String reviewPath = "BookReviews\\reviews.txt";
    private static String SreviewPath = "BookReviews\\Sreviews.txt";
    private  static String bookPath="Books.txt";
    private  static String userPath="Users.txt";
    private  static String suserPath="Susers.txt";
    private  static String pendUserPath="PendUsers.txt";
    private  static String pendBookPath="PendBook.txt";
    private  static String pendComplaintPath="PendComplaint.txt";

    public static ArrayList<User> importUserDB() throws IOException, ClassNotFoundException {
    // read objects from file
        File file = new File(userPath);
        ArrayList<User> temp = new ArrayList<>();

        FileInputStream fi = new FileInputStream(file);
        ObjectInputStream input;
        try{
        input = new ObjectInputStream(fi);
        }
        catch (Exception e){
            return new ArrayList<>(1);
        }

        try {
            while (true) {
                User t = (User) input.readObject();
                temp.add(t);
            }
        } catch (EOFException ex) {
        }
        return temp;
    }
      
    public static ArrayList<Suser> importSUserDB() throws IOException, ClassNotFoundException {
    // read objects from file
        File file = new File(suserPath);
        ArrayList<Suser> temp = new ArrayList<>();

        FileInputStream fi;
        try{
        fi= new FileInputStream(file);}
                catch (Exception e){
            return new ArrayList<>();
        }

        ObjectInputStream input = new ObjectInputStream(fi);

        try {
            while (true) {
                Suser t = (Suser) input.readObject();
                temp.add(t);
            }
        } catch (EOFException ex) {
        }
        return temp;
    }

    public static ArrayList<Book> importBookDB() throws IOException, ClassNotFoundException {
        File file = new File(bookPath);
        ArrayList<Book> temp = new ArrayList<>();
        FileInputStream fi ;
  try{
              fi= new FileInputStream(file);}
              catch (Exception e){
            return new ArrayList<>();
        }

        ObjectInputStream input = new ObjectInputStream(fi);
        try {
            while (true) {
                Book t = (Book) input.readObject();
                temp.add(t);
            }
        } catch (EOFException ex) {
        }
        return temp;
    }
    public static ArrayList<Complaint> importPendComplaintDB() throws IOException, ClassNotFoundException {
    // read objects from file
        File file = new File(pendComplaintPath);
        ArrayList<Complaint> temp = new ArrayList<>();
        FileInputStream fi;
        try {
         fi = new FileInputStream(file);}
        catch (Exception e){
            return new ArrayList<>();
        }
        ObjectInputStream input = new ObjectInputStream(fi);
        try {
            while (true) {
                Complaint t = (Complaint) input.readObject();
                temp.add(t);
            }
        } catch (EOFException ex) {
        }
        return temp;
    }

    public static void UpdateUserDB(ArrayList<User> Users) throws FileNotFoundException, IOException {
    // write objects to file
        File file = new File(userPath);
        try (FileOutputStream fo = new FileOutputStream(file); 
                ObjectOutputStream Output = new ObjectOutputStream(fo)) {
            for (User t : Users) {
                Output.writeObject(t);
                
            }
        }

    }
    
    public static void UpdatePendComplaintDB(ArrayList<Complaint> c) throws FileNotFoundException, IOException {
    // write objects to file
        File file = new File(pendComplaintPath);
        try (FileOutputStream fo = new FileOutputStream(file); 
                ObjectOutputStream Output = new ObjectOutputStream(fo)) {
            for (Complaint t : c) {
                Output.writeObject(t);
                
            }
        }

    }

    public static void UpdateSUserDB( ArrayList<Suser> Susers) throws FileNotFoundException, IOException {
    // write objects to file
        File file = new File(suserPath);
        try (FileOutputStream fo = new FileOutputStream(file); 
                ObjectOutputStream Output = new ObjectOutputStream(fo)) {
            for (Suser t : Susers) {
                Output.writeObject(t);
                
            }
        }
 }

public static void UpdateBookDB(ArrayList<Book> Catalog) throws FileNotFoundException, IOException {
// write objects to file
        File file = new File(bookPath);
        try (FileOutputStream fo = new FileOutputStream(file); 
                ObjectOutputStream Output = new ObjectOutputStream(fo)) {
            for (Book t : Catalog) {
                Output.writeObject(t);
                
            }
        }

    }


public static ArrayList<User> importPendUserDB() throws IOException, ClassNotFoundException {
// write objects to file
    String temp = userPath;
    userPath = pendUserPath;
    ArrayList<User> user =  importUserDB();
    userPath = temp;
    return user;
}

public static void UpdatePendUserDB(ArrayList<User> Catalog) throws FileNotFoundException, IOException {
// write objects to file
    String temp = userPath;
    userPath = pendUserPath;
    UpdateUserDB(Catalog);
    userPath = temp;

}
public static ArrayList<Book> importPendBookDB() throws IOException, ClassNotFoundException {
// write objects to file
    String temp = bookPath;
    bookPath = pendBookPath;
    ArrayList<Book> user =  importBookDB();
    bookPath = temp;
    return user;
}

public static void UpdatePendBookDB(ArrayList<Book> Catalog) throws FileNotFoundException, IOException {
// write objects to file
    String temp = bookPath;
    bookPath = pendBookPath;
    UpdateBookDB(Catalog);
    bookPath = temp;

}
public static boolean CheckLogIn(int uid, String Password, ArrayList<User> users) throws IOException, ClassNotFoundException{
// check regular user password for uid
    if (uid> users.size()-1) return false;
    System.out.println(Password + "  " +users.get(uid).getPword());
    return Password == null ? users.get(uid).getPword() == null : Password.equals(users.get(uid).getPword());
}
public static boolean CheckLogInSuser(int uid, String Password, ArrayList<Suser> users) throws IOException, ClassNotFoundException{
    // check super user password for uid
    if ((uid > users.size()-1) && (uid <0)) return false;
   System.out.println(Password + "  " +users.get(uid).getPword());
  return Password == null ? users.get(uid).getPword() == null : Password.equals(users.get(uid).getPword());
} 
public static ArrayList<Review>  importSReviews(int BookID) throws IOException, FileNotFoundException, ClassNotFoundException{
   // import reviews for each book from suser, each book has file of reviews
   String temp =reviewPath;
    reviewPath = SreviewPath;
    ArrayList<Review>  x =importReviews(BookID);
    reviewPath = temp;
    return x;
}
public static ArrayList<Review> importReviews(int BookID) throws FileNotFoundException, IOException, ClassNotFoundException{
   // import reviews for each book from regular suser, each book has file of reviews
    File file = new File(reviewPath+BookID);
    
    ArrayList<Review> r = new ArrayList<>();
    FileInputStream  fi;
    
        ObjectInputStream input;
           try{
        fi= new FileInputStream(file);
        input = new ObjectInputStream(fi);
        }
        catch (Exception e){
            File f = new File(reviewPath);
            return (new ArrayList<>());
        }

        try {
            while (true) {
                Review t = (Review) input.readObject();            
                if(t.getBookID()== BookID)
                r.add(t);
            }
        } catch (EOFException ex) {
        }
    
        return r;
}
 

public static boolean checkReview(ArrayList<Review> r, int Reviewerid) throws FileNotFoundException, IOException{
   // check if book has review from a user 

    
        for (int i = 0; i < r.size();i++)
        {
            
         if(r.get(i).getReviewerID()== Reviewerid)   
                return false;
            
            
        }
     
               return true;
    
}
public static void updateSReviewsDB(ArrayList<Review> reviews, int bookID) throws IOException{
// write to file
    String temp =reviewPath;
    reviewPath = SreviewPath;
    updateReviewsDB(reviews, bookID);
    reviewPath = temp;
}
public static void updateReviewsDB(ArrayList<Review> reviews, int bookID) throws FileNotFoundException, IOException{
// write to file
    File file = new File(reviewPath+bookID);
        try (FileOutputStream fo = new FileOutputStream(file); 
                ObjectOutputStream Output = new ObjectOutputStream(fo)) {
            for (Review t : reviews) {
                Output.writeObject(t);
                
            }
        }


}

    static boolean checkReviewSuser(ArrayList<Review> r, int Reviewerid) {
      // check if book has review from a suser
    for (int i = 0; i < r.size();i++)
           {

            if(r.get(i).getReviewerID()== Reviewerid)   
                   return false;


           }

                  return true;
    }

}
