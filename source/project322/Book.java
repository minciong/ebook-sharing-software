
package project322;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class Book implements Serializable {
    private int id;
    private String title;
    private String author;
    private String summary;
    private String cover;
    private String file;
    private int complaintCount;
    private int contributer;
    private boolean isSuser= false;

    // compaints, notread,
    
    public Book(int nid, String ntitle, String nauthor, String nsummary, String ncover, String nfile, int cont , boolean isuser){
    this.id=nid;
    this.title=ntitle;
    this.author=nauthor;
    this.summary=nsummary;
    this.complaintCount = 0;
    this.cover=ncover;
    this.file=nfile;
    this.contributer=cont;
    this.isSuser = isuser;
    System.out.println("Book '"+this.title+"' by "+this.author+" added to collection");
    }
    
    public void setId(int nid){this.id=nid;}
    public void setTitle(String ntitle){this.title=ntitle;}
    public void setAuthor(String nAuthor){this.author=nAuthor;}
    public void setSummary(String nSum){this.summary=nSum;}
    
    public void setCover(String nCover){this.cover=nCover;}
    public void setFile(String nFile){this.file=nFile;}
    public void setContributer(User nCont){this.contributer=nCont.getUid();}
    
    public int getId(){return this.id;}
    public String getTitle(){return this.title;}
    public String getAuthor(){return this.author;}
    public String getSummary(){return this.summary;}
    
    public void setcomplaintCount(){complaintCount+=1;}
    public int getcomplaintCount(){return complaintCount;}

    
    public String getCover(){return this.cover;}
    public String getFile(){return this.file;}
    public int getContributer(){return this.contributer;}
    public boolean isSuser(){return isSuser;}
};
