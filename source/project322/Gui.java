
/*
 * you call this class to create default page
 */
package project322;

/**
 *
 * @author binod
 */
import java.util.ArrayList;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public final class Gui extends JFrame {

    // create the frames, buttons necessary in the window
    JPanel books;// has everything related to books, registration panel etc
    JScrollPane base;
    loginpanel login_panel;  // for logging in
    JPanel login_area;          // has logged in buttons
    RegisterPanel registration; // has registration stuff
    loggedInGui loggedIn;     // if logged in then this gui is put in login area
    loggedInGuiSuser loggedInS; // for super user
    int loggedinUid = -1; // uid of logged in user
    boolean logged; // if logged in or not
    boolean suser; // if suser or not
    Object currUser; // current user object

    Gui() {
    }

    Gui(String title) throws IOException, ClassNotFoundException {  // it should later call list of books and stuff to put in
        books = new JPanel();
        base = new JScrollPane(books);
        registration = new RegisterPanel();
        login_panel = new loginpanel();
        login_area = new JPanel();
        loggedIn = new loggedInGui();
        logged = false;
        this.setSize(900, 900); // set size of this window
        ArrayList<Book> catalog = new ArrayList<>();
        initialize(catalog); // creates home page
    }

    public void initialize(ArrayList<Book> catalog) throws IOException, ClassNotFoundException {
        // set up login panel 
        catalog = Data.importBookDB(); // im[port catalog
        books.setLayout(new BoxLayout(books, BoxLayout.Y_AXIS));
        ListToPanel(catalog);  // creates frame for each book and adds it to books which is in base
        login_area.add(login_panel); // add 
        this.add(login_area, BorderLayout.NORTH); // add login on default page on begining
        this.add(base, BorderLayout.CENTER);   // add base for book list display
        books.setBorder(new LineBorder(new Color(0, 0, 0), 2)); // gui stuff
        base.setBackground(Color.red);
        //Data.UpdateBookDB(catalog);
        buttonAction();  // create actions for buttons
        // do not change very important
        pack(); // creates above stuff on gui
        setVisible(true); // gui is visible
        repaint(); // refresh frame
        //setExtendedState(MAXIMIZED_BOTH); // open maximized window
        setDefaultCloseOperation(EXIT_ON_CLOSE); // close program on close button
    }

    public void ActionListenerBooks(final JLabel l, Book B, String order, Gui g) {
      // this sets up actions that happen when we click book pictures
      // remove old stuff from books panel
      // add new stuff in books panel
      l.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                bookInfoGui in = new bookInfoGui(B, g);  // creates gui for book info with all the buttons
                ActionListenerBooks(in.bookPicture, B, "open book", g);
                switch (order) {
                    case "book info page":  // if label is from list of books it opens book info
                        System.out.println(B.getTitle());
                        books.removeAll();
                        books.add(in);// add book info gui
                        break;
                    case "open book":
                        if (logged) {
                            System.out.println(B.getTitle());
                            books.removeAll();
                            ArrayList users = new ArrayList();
                            // check which type of user is logged in 
                            try {
                                if (suser)
                                    users = Data.importSUserDB();
                                else
                                    users = Data.importUserDB();
                            } catch (IOException | ClassNotFoundException ex) {
                                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            pdfopener ebook = null;
                            try {
                                ebook = new pdfopener(B.getFile(), loggedinUid, g, in, B,false); // open pdf
                                books.add(ebook); // add to book
                            } catch (URISyntaxException | IOException | ClassNotFoundException e5) {
                            }
                            
                        } else {
                            login_panel.login.setText(" need to log in to open"); // if not logged in cant open
                        }
                        break;
                }
                pack();
                setVisible(true);
                repaint();

            }

            @Override
            public void mouseClicked(MouseEvent arg0) {
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }
        });
    }
    // same thing as above but for shared books
    public void ShareActionListenerBooks(final JLabel l, Book B, String order, Gui g) {
        l.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                shareBookInfo in = new shareBookInfo(B, g);
                ShareActionListenerBooks(in.bookPicture, B, "open book", g);
                switch (order) {
                    case "book info page":
                        System.out.println(B.getTitle());
                        books.removeAll();
                        books.add(in);
                        break;
                    case "open book":
                        if (logged) {
                            System.out.println(B.getTitle());
                            books.removeAll();
                            ArrayList users = new ArrayList();
                            try {
                                if (suser)
                                    users = Data.importSUserDB();
                                else
                                    users = Data.importUserDB();
                            } catch (IOException | ClassNotFoundException ex) {
                                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            pdfopener ebook = null;
                            ebook = new pdfopener(B.getFile(), loggedinUid, g, in, B,true);
                            books.add(ebook);
                        } else {
                            login_panel.login.setText(" need to log in to open");
                        }
                        break;
                }
                pack();
                setVisible(true);
                repaint();

            }

            @Override
            public void mouseClicked(MouseEvent arg0) {
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
            }
        });
    }
// call home page, lists all book catalog on books
    public void front() {
        try {
            books.removeAll();
            ArrayList<Book> b = Data.importBookDB(); // read book db and
            ListToPanel(b); // add to books
            pack(); // creates above stuff on gui
            repaint();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
// button actions
    public void buttonAction() {
        login_panel.login.addActionListener((ActionEvent e) -> {
            try {
                ArrayList<User> users = Data.importUserDB();
                if (login_panel.isSuser.isSelected()) { // if suser checked    check suser db           
                    try {
                        ArrayList<Suser> susers = Data.importSUserDB();

                        try {
                            loggedinUid = Integer.parseInt(login_panel.username.getText()); // check uid is integer
                            logged = Data.CheckLogInSuser(loggedinUid, String.valueOf(login_panel.password.getPassword()), susers);

                        } catch (NumberFormatException ne) {
                            logged = false; // if uid or password false then not logged
                        }
                        if (logged) {  // if logged call logged in gui with options 
                            suser = true;
                            currUser = susers.get(loggedinUid); // set current user
                            try {
                                login_area.removeAll(); // update login area with buttons
                                loggedIn.label.setText("welcome user: " + ((Suser) currUser).fName + " " + ((Suser) currUser).lName);
                                // display stuff
                                loggedIn.uid.setText("User Id: " + loggedinUid + "\t");
                                loggedInS = new loggedInGuiSuser(this);
                                loggedInS.initlabels(); //initiates stuff for jpanel
                                loggedIn.points.setText("Points : " +((Suser) currUser).getPoints()+"\t" );
                                // set actions for button                         
                                login_area.add(loggedInS);  // suser options
                                login_area.add(loggedIn);

                            } catch (ClassNotFoundException ex) {
                                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        } else {
                            loggedinUid = -1;
                            login_panel.login.setText(" enter correct username and password and click");
                        }
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else { /// for regular user same stuff except ssuser optiuons
                    try {
                        loggedinUid = Integer.parseInt(login_panel.username.getText());
                        logged = Data.CheckLogIn(loggedinUid, String.valueOf(login_panel.password.getPassword()), users);

                    } catch (NumberFormatException ne) {
                        logged = false;
                        suser = false;
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (logged) {
                        currUser = (User) users.get(loggedinUid);
                        loggedIn.points.setText("Points : " +((User) currUser).getPoints()+"\t" );     
                        login_area.removeAll();
                        loggedIn.label.setText("welcome user " + ((User) currUser).fName + " " + ((User) currUser).lName);
                        // set actions for button
                        login_area.add(loggedIn);
                        loggedIn.uid.setText("User Id: " + loggedinUid + "");

                    } else {
                        loggedinUid = -1;
                        login_panel.login.setText(" enter correct username and password and click");

                    }
                    setSize(900, 900);
                }
                pack(); // creates above stuff on gui
                setVisible(true); // gui is visible
                repaint(); // refresh frame
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        login_panel.register.addActionListener((ActionEvent e) -> { // button action
            books.removeAll();
            books.add(registration); // show gui
            pack(); // creates above stuff on gui
            setVisible(true); // gui is visible
            repaint();
        });

        login_panel.browse.addActionListener((ActionEvent e) -> {
            front(); //show list of books
        });
        loggedIn.browse.addActionListener((ActionEvent e) -> {
            front(); //show list of books
        });

        loggedIn.reccomended.addActionListener((ActionEvent e) -> {
           books.removeAll();
            if (suser) {
                try {
                    ListToPanel(Data.importSUserDB().get(this.loggedinUid).getRecommended()); // read recommended
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    ListToPanel(Data.importUserDB().get(this.loggedinUid).getRecommended()); // read recommended
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            repaint();
        });
        loggedIn.favourites.addActionListener((ActionEvent e) -> {
            books.removeAll();
            if (suser) {
                try {
                    ListToPanel(Data.importSUserDB().get(this.loggedinUid).getFavorites());// get favourites for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    ListToPanel(Data.importUserDB().get(this.loggedinUid).getFavorites());// get favourites for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            repaint();
        });
        loggedIn.history.addActionListener((ActionEvent e) -> {
            books.removeAll();
            if (suser) {
                try {
                    ListToPanel(Data.importSUserDB().get(this.loggedinUid).getHistory());// get history for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    ListToPanel(Data.importUserDB().get(this.loggedinUid).getHistory());// get history for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
 }
            pack(); // creates above stuff on gui
            setVisible(true); // gui is visible
            repaint();
        });

        loggedIn.shared.addActionListener((ActionEvent e) -> {
            books.removeAll();
            if (suser) {
                try {
                    SharePanel(Data.importSUserDB().get(this.loggedinUid).getShared());// get shared for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    SharePanel(Data.importUserDB().get(this.loggedinUid).getShared());// get shared for current uid
                } catch (IOException | ClassNotFoundException ex) {
                    Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            repaint();
        });
        loggedIn.logOut.addActionListener((ActionEvent e) -> {
        // logout resets every thing and shows catalog
            this.login_area.removeAll();
            this.books.removeAll();
            this.login_area.add(this.login_panel);
            this.logged = false;
            this.suser = false;
            this.currUser = null;
            try {
                ArrayList<Book> catalog = Data.importBookDB();
                this.ListToPanel(catalog);
                base.repaint();
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(loggedInGuiSuser.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.pack();
            this.repaint();

        });
        loggedIn.submitBook.addActionListener((ActionEvent e) -> {
        // calls submit book gui
            this.books.removeAll();
            submitBook s = new submitBook(this.loggedinUid, suser);
            this.books.add(s);
            this.pack();
            this.repaint();
        });
    }

    public void ListToPanel(ArrayList<Book> catalog) {
    // creates a frame for each book and adds to book
        books.removeAll();
        pack();
        repaint();
        System.out.println("added");
        catalog.stream().map((catalog1) -> {
            String title_book = catalog1.getTitle();
            String summary_book = catalog1.getSummary();
            String book_cover_address = "BookCover/" + catalog1.getCover();
            bookpanelGui bk = new bookpanelGui(title_book, book_cover_address, summary_book);// set action for clicking book cover
            ActionListenerBooks(bk.bookPicture, catalog1, "book info page", this);
            return bk;
        }).forEach((bk) -> {
            books.add(bk, new GridLayout());
            try {
                books.repaint();
                Thread.sleep(10);                 //1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            pack();
            repaint();
            setSize(900, 900);
        });
       

    }
    public void SharePanel(ArrayList<Book> catalog) {
    // same thing as above but for shared books
        books.removeAll();
        pack();
        repaint();
        System.out.println("added");
        catalog.stream().map((catalog1) -> {
            String title_book = catalog1.getTitle();
            String summary_book = catalog1.getSummary();
            String book_cover_address = "BookCover/" + catalog1.getCover();
            shareBookPanel bk = new shareBookPanel(title_book, book_cover_address, summary_book);
            ShareActionListenerBooks(bk.bookPicture, catalog1, "book info page", this); // set action for clicking book cover
            return bk;
        }).forEach((bk) -> {
            books.add(bk, new GridLayout());
            try {
                books.repaint();
                Thread.sleep(10);                 //1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            pack();
            repaint();
            setSize(900, 900);
        });
      

    }
}
